package biblioteca.repository.repo;

import biblioteca.model.Carte;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertTrue;

public class BibliotecaRepoTest {
    private CartiRepoMock cartiRepo=new CartiRepoMock();
    private static Map<Integer,List<Carte>> testsSetUp=new HashMap<Integer, List<Carte>>();
    private int key;

    @Test
    public void test1(){
        assertTrue(cartiRepo.cautaCarte("abc").size()==0);
    }

    @Test
    public void test2(){
        List<Carte> carti=cartiRepo.cautaCarte("escu");
        assertTrue(carti.size()==3);
        assertTrue(carti.get(0).getReferenti().get(0).toLowerCase().contains("escu"));
        assertTrue(carti.get(1).getReferenti().get(0).toLowerCase().contains("escu"));
        assertTrue(carti.get(2).getReferenti().get(0).toLowerCase().contains("escu"));
    }

    @Test
    public void test3(){
        assertTrue(cartiRepo.cautaCarte("abc").size()==0);
    }

    @Test
    public void test4(){
        List<Carte> carti=cartiRepo.cautaCarte("ale");
        assertTrue(carti.size()==3);
        assertTrue(carti.get(0).getReferenti().get(1).toLowerCase().contains("ale"));
        assertTrue(carti.get(1).getReferenti().get(0).toLowerCase().contains("ale"));
        assertTrue(carti.get(2).getReferenti().get(0).toLowerCase().contains("ale"));
    }

    @Test
    public void test5(){
        try {
            cartiRepo.cautaCarte(null);
            assertTrue(false);
        }catch (NullPointerException ex){
            assertTrue(true);
        }
    }
}
