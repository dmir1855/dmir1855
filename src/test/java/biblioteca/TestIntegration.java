package biblioteca;

import biblioteca.integration.BigBangTest;
import biblioteca.integration.TopDownTest;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

/**
 * Created by IT on 22/04/2018.
 */
public class TestIntegration {
    @Test
    public void TestBigBang(){
        Result result = JUnitCore.runClasses(BigBangTest.class);

        for (Failure failure : result.getFailures()) {
            System.out.println(failure.toString());
        }

        System.out.println(result.wasSuccessful());
    }

    @Test
    public void TestTopDown(){
        Result result = JUnitCore.runClasses(TopDownTest.class);

        for (Failure failure : result.getFailures()) {
            System.out.println(failure.toString());
        }

        System.out.println(result.wasSuccessful());
    }
}