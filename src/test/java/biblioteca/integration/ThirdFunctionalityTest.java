package biblioteca.integration;

import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.repository.repo.CartiRepo;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class ThirdFunctionalityTest {
    private BibliotecaCtrl bibliotecaCtrl=new BibliotecaCtrl(new CartiRepo());

    @Test
    public void BBT_Valid(){
        List<Carte> carti;
        try{
            carti=bibliotecaCtrl.getCartiOrdonateDinAnul("1948");
            assertTrue(carti.size()==4);
        }catch (Exception ex){
            assertTrue(false);
        }
    }

    @Test
    public void BBT_Nonvalid(){
        List<Carte> carti;
        try{
            carti=bibliotecaCtrl.getCartiOrdonateDinAnul("19b");
            assertTrue(false);
        }catch (Exception ex){
            assertTrue(true);
        }
    }
}
