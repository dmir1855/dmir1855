package biblioteca.control;

import biblioteca.model.Carte;
import biblioteca.repository.repo.CartiRepo;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class BibliotecaCtrlTest {
    CartiRepo cr = new CartiRepo();
    BibliotecaCtrl repo = new BibliotecaCtrl(cr);
    ArrayList<String> reff = new ArrayList<String>();
    ArrayList<String> keys = new ArrayList<String>();

    @Test
    public void adaugaCarte() throws Exception {
        reff.add("Cassandra Clare");
        keys.add("Fantasy");
        keys.add("Young Adult");
        Carte carte = new Carte("Ingerul mecanic", reff, "2010", keys, "Leda");

        repo.adaugaCarte(carte);
        Carte last = repo.getCarti().get(repo.getCarti().size()-1);
        assertEquals("Titlul difera", carte.getTitlu(), last.getTitlu());
        assertEquals("Nu este acelasi numar de autori", carte.getReferenti().size(), last.getReferenti().size());
        assertEquals("Anul aparitiei!!", carte.getAnAparitie(), last.getAnAparitie());
        assertEquals("Cuv cheie", carte.getCuvinteCheie().size(), last.getCuvinteCheie().size());
        assertEquals("skbcsj", carte.getEditura(), last.getEditura());
    }

    @Test
    public void add2() throws Exception{
        reff.add("Cassandra Clare");
        keys.add("Fantasy");
        keys.add("Young Adult");
        Carte carte = new Carte("", reff, "2010", keys, "Leda");

        try {
            repo.adaugaCarte(carte);
            fail("Exception expected from " + carte.getTitlu());
        } catch (Exception e) {
            // fine, as expected
        }
    }

    @Test
    public void add3() throws Exception{
        reff.add("Autor a");
        reff.add("Autor b");
        keys.add("keyword a");
        keys.add("keyword b");
        Carte carte = new Carte("M", reff, "201q", keys, "Leda");

        try {
            repo.adaugaCarte(carte);
            fail("Exception expected from " + carte.getAnAparitie());
        } catch (Exception e) {
            // fine, as expected
        }
    }

    @Test
    public void add4() throws Exception{
        reff.add("Autor a");
        reff.add("Autor b");
        keys.add("keyword a");
        keys.add("keyword b");
        Carte carte = new Carte("Mxy", reff, "2019", keys, "Leda");

        try {
            repo.adaugaCarte(carte);
            fail("Exception expected from " + carte.getReferenti());
        } catch (Exception e) {
            // fine, as expected
        }
    }
}