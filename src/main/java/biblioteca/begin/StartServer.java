package biblioteca.begin;

import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.repository.repo.CartiRepo;
import com.google.gson.Gson;
import org.glassfish.grizzly.http.server.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import org.apache.commons.io.IOUtils;

import static org.glassfish.grizzly.http.Method.GET;
import static org.glassfish.grizzly.http.Method.POST;


public class StartServer {

    private static class CartiRequestHandler {

        private static Gson g = new Gson();

        private static void setDefaultHeaders(Response response) {
            response.setHeader("Access-Control-Allow-Origin", "*");
        }

        static void HandleGET(BibliotecaCtrl controller, Request request, Response response) throws  Exception {
            List<Carte> carti = controller.getCarti();

            setDefaultHeaders(response);

            response.setHeader("Content-Type", "application/json");
            OutputStreamWriter writer = new OutputStreamWriter(response.getOutputStream());
            writer.write(g.toJson(carti));
            writer.close();

            response.setStatus(200);
            response.finish();
        }

        static void HandlePOST(BibliotecaCtrl controller, Request request, Response response) throws Exception {

            setDefaultHeaders(response);

            POSTRequest body = g.fromJson(new InputStreamReader(request.getInputStream()), POSTRequest.class);
            try {
                controller.adaugaCarte(body.c);
            } catch(Exception e) {
                response.sendError(400);
                return;
            }

            response.setStatus(200);
            response.finish();
        }

        private static class POSTRequest {
            Carte c;

            public Carte getCarte() {
                return c;
            }

            public void setCarte(Carte c) {
                this.c = c;
            }
        }

    }

    public static void main(String[] args) {
        HttpServer server = HttpServer.createSimpleServer(".", 1234);
        final BibliotecaCtrl controller = new BibliotecaCtrl(new CartiRepo());

        try {

            server.start();

            server.getServerConfiguration().addHttpHandler(
                    new HttpHandler() {
                        @Override
                        public void service(Request request, Response response) throws Exception {
                            try {
                                if (request.getMethod() == GET) {
                                    CartiRequestHandler.HandleGET(controller, request, response);
                                } else if (request.getMethod() == POST) {
                                    CartiRequestHandler.HandlePOST(controller, request, response);
                                }
                            } catch(Exception e) {
                                response.sendError(500);
                            }
                        }
                    }, "/carti");

            server.getServerConfiguration().addHttpHandler(
                    new HttpHandler() {
                        @Override
                        public void service(Request request, Response response) throws Exception {
                            if(request.getMethod() == GET) {

                                ClassLoader classloader = Thread.currentThread().getContextClassLoader();
                                InputStream inData = classloader.getResourceAsStream("page.html");

                                IOUtils.copy(inData, response.getOutputStream());

                                response.setStatus(200);
                                response.finish();
                            }
                        }
                    }, "/home");

            System.out.println("Press any key to stop");
            System.in.read();

        }catch(Exception e) {
            e.printStackTrace();
        }
    }

}
